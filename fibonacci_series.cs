// What is Fibonacci Series?
// Fibonacci series is a sequence of numbers in below order:
// 0, 1, 1, 2, 3, 5, 8, 13, 21, 34… The next number is found by adding up the two numbers before it.

// The formula for calculating these numbers is:

// F(n) = F(n-1) + F(n-2)

// where:

// F(n) is the term number.
// F(n-1) is the previous term (n-1).
// F(n-2) is the term before that (n-2).

// it starts either with 0 or 1.
 

// Different ways to print Fibonacci Series in C#?
// In C#, there are several ways to print Fibonacci Series.

// Iterative Approach
// Recursion Approach



// Iterative Approach : This is the simplest way of generating Fibonacci seres in C#.

namespace ConsoleApplication
{
    class Program
    {
        static int FibonacciSeries(int n)
        {
            int firstnumber = 0, secondnumber = 1, result = 0;

            if (n == 0) return 0; //To return the first Fibonacci number   
            if (n == 1) return 1; //To return the second Fibonacci number   

            for (int i = 2; i <= n; i++)
            {
                result = firstnumber + secondnumber;
                firstnumber = secondnumber;
                secondnumber = result;
            }
            return result;
        }
        static void Main(string[] args)
        {
            Console.Write("Enter the length of the Fibonacci Series: ");
            int length = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < length; i++)
            {
                Console.Write("{0} ", FibonacciSeries(i));
            }
            Console.ReadKey();
        }
    }  
}

//Recursive Approach : In this approach, We need to pass the length of the Fibonacci Series to the recursive method and then it iterates continuously until it reaches the goal.

namespace ConsoleApplication
{
    class Program
    {
        public static int FibonacciSeries(int n)
        {
            if (n == 0) return 0; //To return the first Fibonacci number   
            if (n == 1) return 1; //To return the second Fibonacci number   
            return FibonacciSeries(n - 1) + FibonacciSeries(n - 2);
        }
        public static void Main(string[] args)
        {
            Console.Write("Enter the length of the Fibonacci Series: ");
            int length = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < length; i++)
            {
                Console.Write("{0} ", FibonacciSeries(i));
            }
            Console.ReadKey();
        }
    }
}


//Printing nth fibonacci number in series

namespace ConsoleApplication
{
    class Program
    {
        public static int NthFibonacciNumber(int n)
        {
            if ((n == 0) || (n == 1))
            {
                return n;
            }
            else
            {
                return (NthFibonacciNumber(n - 1) + NthFibonacciNumber(n - 2));
            }
        }

        public static void Main(string[] args)
        {
            Console.Write("Enter the nth number of the Fibonacci Series: ");
            int number = Convert.ToInt32(Console.ReadLine());
            number = number - 1;
            //We have to decrement the length because the series starts with 0  

            Console.Write(NthFibonacciNumber(number));
            Console.ReadKey();
        }
    }
}
